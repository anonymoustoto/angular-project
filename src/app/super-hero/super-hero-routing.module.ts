import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SuperHeroListComponent} from "./components/super-hero-list/super-hero-list.component";
import {SuperHeroDetailsComponent} from "./components/super-hero-details/super-hero-details.component";

const routes: Routes = [
  {
    path: 'list',
    component: SuperHeroListComponent
  },
  {
    path: ':id/details',
    component: SuperHeroDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperHeroRoutingModule {
}
