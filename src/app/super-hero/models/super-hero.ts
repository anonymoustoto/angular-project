export interface SuperHero {
  name: string;
  id: number;
  alterEgo: string;
}
