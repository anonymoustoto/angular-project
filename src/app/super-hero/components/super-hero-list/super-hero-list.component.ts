import {Component, computed, Signal, signal, WritableSignal} from '@angular/core';
import {SuperHero} from "../../models/super-hero";

@Component({
  selector: 'app-super-hero-list',
  templateUrl: './super-hero-list.component.html',
  styleUrl: './super-hero-list.component.scss'
})
export class SuperHeroListComponent {
  totalPage = computed(() => Math.ceil(this.listSuperHero.length / this.selectedPageSize()))
  page: WritableSignal<number> = signal(0);
  selectedPageSize: WritableSignal<number> = signal(4);

  optionPageSize = [4, 8, 12, 16, 20]

  firstElement = computed(() => this.page() * this.selectedPageSize())
  lastElement = computed(() => this.firstElement() + this.selectedPageSize())

  isLastPage = computed(() => this.lastElement() >= this.listSuperHero.length)
  isFirstPage = computed(() => this.firstElement() < 1)

  listSuperHero: SuperHero[] = [
    {name: 'Superman', id: 1, alterEgo: 'Clark Kent'},
    {name: 'Batman', id: 1, alterEgo: 'Bruce Wayne'},
    {name: 'Green Lantern', id: 1, alterEgo: 'Hal Jordan'},
    {name: 'Flash', id: 1, alterEgo: 'Barry Allen'},
    {name: 'Aquaman', id: 1, alterEgo: 'Arthur Curry'},
    {name: 'Wonder Woman', id: 1, alterEgo: 'Diana Prince'},
    {name: 'Martian Manhunter', id: 1, alterEgo: 'J\'onn J\'onzz'},
    {name: 'Green Arrow', id: 1, alterEgo: 'Oliver Queen'},
    {name: 'Atom', id: 1, alterEgo: 'Ray Palmer'},
    {name: 'Hawkman', id: 1, alterEgo: 'Carter Hall'},
    {name: 'Hawkgirl', id: 1, alterEgo: 'Shiera Hall'},
    {name: 'Cyborg', id: 1, alterEgo: 'Victor Stone'},
    {name: 'Supergirl', id: 1, alterEgo: 'Kara Zor-El'},
    {name: 'Shazam', id: 1, alterEgo: 'Billy Batson'},
    {name: 'Zatanna', id: 1, alterEgo: 'Zatanna Zatara'},
    {name: 'Black Canary', id: 1, alterEgo: 'Dinah Drake'},
    {name: 'Doctor Fate', id: 1, alterEgo: 'Kent Nelson'},
    {name: 'Blue Beetle', id: 1, alterEgo: 'Dan Garrett'},
    {name: 'Blue Beetle', id: 1, alterEgo: 'Ted Kord'},
    {name: 'Blue Beetle', id: 1, alterEgo: 'Jaime Reyes'},
    {name: 'Booster Gold', id: 1, alterEgo: 'Michael Jon Carter'},
    {name: 'Firestorm', id: 1, alterEgo: 'Ronnie Raymond'},
    {name: 'Red Tornado', id: 1, alterEgo: 'John Smith'},
    {name: 'Spiderman', id: 1, alterEgo: 'Peter Parker'},
    {name: 'Ironman', id: 1, alterEgo: 'Tony Stark'},
    {name: 'Thor', id: 1, alterEgo: 'Thor Odinson'},
    {name: 'Hulk', id: 1, alterEgo: 'Bruce Banner'},
    {name: 'Captain America', id: 1, alterEgo: 'Steve Rogers'},
    {name: 'Black Widow', id: 1, alterEgo: 'Natasha Romanoff'},
    {name: 'Captain Marvel', id: 1, alterEgo: 'Carol Danvers'},
  ]

  lastPage() {
    this.page.set(this.totalPage() - 1);
  }
  nextPage() {
    this.page.set(this.page() + 1);
  }
  firstPage() {
    this.page.set(0);
  }
  previousPage() {
    this.page.set(this.page() - 1);
  }

  changePageSize(event: Event) {
    this.selectedPageSize.set(parseInt((event.target as HTMLSelectElement).value));
  }
}
