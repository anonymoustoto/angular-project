import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperHeroDetailsComponent } from './super-hero-details.component';

describe('SuperHeroDetailsComponent', () => {
  let component: SuperHeroDetailsComponent;
  let fixture: ComponentFixture<SuperHeroDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SuperHeroDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SuperHeroDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
