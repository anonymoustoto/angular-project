import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-super-hero-details',
  templateUrl: './super-hero-details.component.html',
  styleUrl: './super-hero-details.component.scss'
})
export class SuperHeroDetailsComponent {

  @Input() id!: number;

  constructor() { }
}
