import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperHeroListComponent } from './components/super-hero-list/super-hero-list.component';
import { SuperHeroDetailsComponent } from './components/super-hero-details/super-hero-details.component';
import {SuperHeroRoutingModule} from "./super-hero-routing.module";



@NgModule({
  declarations: [
    SuperHeroListComponent,
    SuperHeroDetailsComponent
  ],
  exports: [
    SuperHeroListComponent,
    SuperHeroDetailsComponent
  ],
  imports: [
    CommonModule,
    SuperHeroRoutingModule
  ]
})
export class SuperHeroModule { }
