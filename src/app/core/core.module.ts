import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutCoreComponent} from './layout/layout-core/layout-core.component';
import {SuperHeroModule} from "../super-hero/super-hero.module";
import {CoreRoutingModule} from "./core-routing.module";
import { PowerModule } from '../power/power.module';
import { FetchBackendService } from './services/fetch-backend/fetch-backend.service';


@NgModule({
  declarations: [
    LayoutCoreComponent
  ],
  exports: [
    LayoutCoreComponent
  ],
  imports: [
    CommonModule,
    SuperHeroModule,
    CoreRoutingModule,
    PowerModule
  ],
  providers: [
    FetchBackendService
  ]
})
export class CoreModule {
}
