import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LayoutCoreComponent} from "./layout/layout-core/layout-core.component";

const routes: Routes = [
  {
    path: '',
    component: LayoutCoreComponent,
    children: [
      {
        path: 'super-heroes',
        loadChildren: () => import('../super-hero/super-hero.module')
          .then(m => m.SuperHeroModule)
      },
      {
        path: 'powers',
        loadChildren: () => import('../power/power.module')
          .then(m => m.PowerModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
