import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../environnements/environnement';
import { throwError, catchError, Observable } from 'rxjs';

type optionalProps<T> = {
  [K in keyof T]?: T[K];
};

@Injectable()

export class FetchBackendService {

  constructor(private http: HttpClient) {}

  #url = 'http://' + environment.backendHost + ':' + environment.backendPort;

  
  get<T>(path : string) {
    return this.http.get<T>(this.#url + path)
      .pipe(
        catchError(this.handleError)
      );
  }

  post<T>(path: string, object : optionalProps<T>): Observable<T> {
    console.log(this.#url + path, object)
    return this.http.post<T>(this.#url + path, object)
      .pipe(
        catchError(this.handleError)
      );
  }

  patch<T>(path: string, object : optionalProps<T>): Observable<T> {
    return this.http.patch<T>(this.#url + path, object)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    console.error(error.status === 0 ? 'An error occurred:' : `Backend returned code ${error.status}, body was: `, error.error);
    return throwError(() => error);
  }
}
