import { TestBed } from '@angular/core/testing';

import { FetchBackendService } from './fetch-backend.service';

describe('RequestPowerService', () => {
  let service: FetchBackendService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchBackendService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
