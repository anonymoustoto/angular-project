import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PowerListComponent } from './components/power-list/power-list.component';
import { PowerRoutingModule } from './power-routing.module.';
import { PowerEditCreateComponent } from './components/power-edit-create/power-edit-create.component';
import { FormsModule } from '@angular/forms';
import { ModalComponent } from './components/modal/modal.component';


@NgModule({
  declarations: [
    PowerListComponent,
    PowerEditCreateComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    PowerRoutingModule,
    FormsModule
  ],
  exports: [
    PowerListComponent,
    PowerEditCreateComponent
  ]
})
export class PowerModule { }
