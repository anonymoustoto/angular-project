import {Component, computed, Signal, signal, WritableSignal, OnInit} from '@angular/core';
import { Power } from '../../models/power';
import { FetchBackendService } from '../../../core/services/fetch-backend/fetch-backend.service';

@Component({
  selector: 'app-power-list',
  templateUrl: './power-list.component.html',
  styleUrl: './power-list.component.scss'
})
export class PowerListComponent {
  constructor(private request: FetchBackendService) {}

  listPower: Power[] = [];
  
  ngOnInit() {
    this.request.get<Power[]>("/api/v1/powers")
      .subscribe(data => this.listPower = data ? data : []);
  }

  totalPage = computed(() => Math.ceil(this.listPower.length / this.selectedPageSize()))
  page: WritableSignal<number> = signal(0);
  selectedPageSize: WritableSignal<number> = signal(4);

  optionPageSize = [4, 8, 12, 16, 20]

  firstElement = computed(() => this.page() * this.selectedPageSize())
  lastElement = computed(() => this.firstElement() + this.selectedPageSize())

  isLastPage = computed(() => this.lastElement() >= this.listPower.length)
  isFirstPage = computed(() => this.firstElement() < 1)

  /* listPower: Power[] = [
    { id: 1, name: 'Super Strength', description: 'Incredible physical strength' },
    { id: 2, name: 'Flight', description: 'Ability to fly at high speeds' },
    { id: 3, name: 'Telekinesis', description: 'Move objects with the mind' },
    { id: 4, name: 'Invisibility', description: 'Ability to become unseen' },
    { id: 5, name: 'Teleportation', description: 'Instantaneous travel from one place to another' },
    { id: 6, name: 'Shape-Shifting', description: 'Ability to change physical form' },
    { id: 7, name: 'Time Manipulation', description: 'Control over the flow of time' },
    { id: 8, name: 'Mind Reading', description: 'Ability to read minds' },
    { id: 9, name: 'Healing Factor', description: 'Rapid healing and regeneration' },
    { id: 10, name: 'Elasticity', description: 'Stretch and deform their body' },
    { id: 11, name: 'Fire Manipulation', description: 'Control and generate fire' },
    { id: 12, name: 'Ice Powers', description: 'Manipulate ice and cold' },
    { id: 13, name: 'Super Speed', description: 'Move at incredibly fast speeds' },
    { id: 14, name: 'X-Ray Vision', description: 'See through objects and materials' },
    { id: 15, name: 'Super Intelligence', description: 'Exceptional cognitive abilities' },
    { id: 16, name: 'Energy Projection', description: 'Emit and control various forms of energy' },
    { id: 17, name: 'Weather Control', description: 'Manipulate weather patterns' },
    { id: 18, name: 'Super Agility', description: 'Exceptional agility and reflexes' },
    { id: 19, name: 'Telepathy', description: 'Communicate using the mind' },
    { id: 20, name: 'Super Senses', description: 'Enhanced senses such as hearing and sight' },
  ]; */

  lastPage() {
    this.page.set(this.totalPage() - 1);
  }
  nextPage() {
    this.page.set(this.page() + 1);
  }
  firstPage() {
    this.page.set(0);
  }
  previousPage() {
    this.page.set(this.page() - 1);
  }

  changePageSize(event: Event) {
    this.selectedPageSize.set(parseInt((event.target as HTMLSelectElement).value));
  }
}
