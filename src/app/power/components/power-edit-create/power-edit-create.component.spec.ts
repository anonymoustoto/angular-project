import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerEditCreateComponent } from './power-edit-create.component';

describe('PowerEditCreateComponent', () => {
  let component: PowerEditCreateComponent;
  let fixture: ComponentFixture<PowerEditCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PowerEditCreateComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PowerEditCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
