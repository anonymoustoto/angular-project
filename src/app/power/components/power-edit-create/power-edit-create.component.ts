import { Component, OnInit } from '@angular/core';
import { FetchBackendService } from '../../../core/services/fetch-backend/fetch-backend.service';
import { Power } from '../../models/power';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-power-edit',
  templateUrl: './power-edit-create.component.html',
  styleUrl: './power-edit-create.component.scss'
})
export class PowerEditCreateComponent {
  power? : Power;
  powerUpdated : Power = {name : '', description : ''};
  id? : string;
  path = "/api/v1/powers";

  constructor(private request: FetchBackendService, private route: ActivatedRoute, private router : Router, private modalService: NgbModal) {}
  
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id') ?? undefined;
    if(this.id) {
      this.request.get<Power>(this.path + '/' + this.id)
        .subscribe({
          next : data => {this.power = {...data}; this.powerUpdated = {...data}},
          error : error => this.router.navigate(['powers', 'list'])
        });
    }
  }

  updatePower() {
    if(this.powerUpdated?.description || this.powerUpdated?.name || (this.power && !(this.powerUpdated.name + this.powerUpdated.description === this.power.name + this.power.description))) {
      this.request.patch<Power>(this.path + '/' + this.id, this.powerUpdated)
        .subscribe({
          next : data => {this.power = {...data}; this.powerUpdated = {...data}; this.showModal('Modifié avec succès', '#00C851'); },
          error : error =>  {this.showModal(error.message, '#EF9A9A')}
        });
    }
    else {
      this.showModal('Informations manquantes ou incorrectes', '#EF9A9A');
    }
  }

  createPower() {
    if(this.powerUpdated?.description && this.powerUpdated?.name) {
      this.request.post<Power>(this.path, this.powerUpdated)
        .subscribe({
          next : data => {this.showModal('Ajouté avec succès', '#00C851'); this.router.navigate(['powers', data.id, 'edit'])},
          error : error =>  {this.showModal(error.message, '#EF9A9A')}
        });
    }
    else {
      this.showModal('Informations manquantes ou incorrectes', '#EF9A9A');
    }
  }

  showModal(msg : string, color?: string) {
    const modal = this.modalService.open(ModalComponent);
    modal.componentInstance.message = msg;
    modal.componentInstance.color = color;
    setTimeout(() => {this.modalService.dismissAll()}, 1000)
  }
}
