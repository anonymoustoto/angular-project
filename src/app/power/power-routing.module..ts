import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PowerListComponent} from "./components/power-list/power-list.component";
import { PowerEditCreateComponent } from './components/power-edit-create/power-edit-create.component';
/* import {SuperHeroDetailsComponent} from "./components/super-hero-details/super-hero-details.component"; */

const routes: Routes = [
  {
    path: 'list',
    component: PowerListComponent
  },
  {
    path: ':id/edit',
    component: PowerEditCreateComponent
  },
  {
    path: 'add',
    component: PowerEditCreateComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PowerRoutingModule {
}
